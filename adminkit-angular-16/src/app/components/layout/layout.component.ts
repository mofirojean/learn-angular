import { Component } from '@angular/core';

@Component({
  selector: 'app-layout',
  template: `
    <div class="wrapper">
      <app-sidebar></app-sidebar>
      <div class="main">
        <app-header></app-header>
        <main class="content" >
          <div class="container-fluid p-0">
            <router-outlet></router-outlet>
          </div>
          <app-footer></app-footer>
        </main>
      </div>
    </div>
  `,
  styles: [
    `
    div.wrapper {
      height: 100vh;
    }
    div.main, app-sidebar {
      height: 100%;
    }
    app-header {
      height: 10%;
    }
    main.content {
      height: 90%;
      overflow-y: auto;
    }
    `
  ]
})
export class LayoutComponent {

}
