export interface NavItem {
  header: string,
  items: NavItemType[]
}

export interface NavItemType {
  title: string
  icon: string
  link: string
}

export const NavItems: NavItem[] = [
  {
    header: 'Pages',
    items: [
      {
        title: 'Dashboard',
        icon: 'sliders',
        link: '/app/admin/dashboard'
      },
      {
        title: 'Profile',
        icon: 'user',
        link: '/app/admin/profile'
      },
      {
        title: 'Sign Up',
        icon: 'user-plus',
        link: '/auth/register'
      },
      {
        title: 'Sign In',
        icon: 'log-in',
        link: '/auth/login'
      },
      {
        title: 'Blank',
        icon: 'book',
        link: '/app/admin/blank-page'
      }
    ]
  },
  {
    header: 'Tools & Components',
    items: [
      {
        title: 'Buttons',
        icon: 'square',
        link: '/app/admin/buttons'
      },
      {
        title: 'Forms',
        icon: 'check-square',
        link: '/app/admin/forms'
      },
      {
        title: 'Cards',
        icon: 'grid',
        link: '/app/admin/cards'
      },
      {
        title: 'Typography',
        icon: 'align-left',
        link: '/app/admin/typography'
      },
      {
        title: 'Icons',
        icon: 'coffee',
        link: '/app/admin/icons'
      }
    ]
  },
  {
    header: 'Plugins & Addons',
    items: [
      {
        title: 'Charts',
        icon: 'bar-chart-2',
        link: '/app/admin/charts'
      },
      {
        title: 'Maps',
        icon: 'map',
        link: '/app/admin/maps'
      }
    ]
  }
]
