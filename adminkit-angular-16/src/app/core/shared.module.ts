import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibraryModule } from "./library/library.module";
import { IconsModule } from "./library/icons/icons.module";



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    LibraryModule,
    IconsModule
  ]
})
export class SharedModule { }
