import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './navigations/header/header.component';
import { SidebarComponent } from './navigations/sidebar/sidebar.component';
import { FooterComponent } from './navigations/footer/footer.component';
import {RouterLink, RouterLinkActive} from "@angular/router";
import {IconsModule} from "./icons/icons.module";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";



@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent
  ],
  exports: [
    SidebarComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterLink,
    RouterLinkActive,
    IconsModule,
    NgbDropdownModule
  ]
})
export class LibraryModule { }
