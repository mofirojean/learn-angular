import { Component } from '@angular/core';
import {NavItem, NavItems} from "../../../../utils/nav.utils";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
    `
    a.sidebar-hover:hover {
      text-decoration: none;
    }
    `
  ]
})
export class SidebarComponent {
  navItems: NavItem[] = NavItems;
}
