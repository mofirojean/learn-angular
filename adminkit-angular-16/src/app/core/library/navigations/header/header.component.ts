import {Component} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  collapseSidebarMenuAction = () => {
    const sidebarElement = document.querySelector('nav.sidebar');
    if (sidebarElement?.classList.contains('collapsed')) {
      sidebarElement?.classList.remove('collapsed');
    } else {
      sidebarElement?.classList.add('collapsed')
    }
  }
}
