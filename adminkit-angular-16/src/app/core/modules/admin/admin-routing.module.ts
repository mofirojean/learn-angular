import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdmDashboardComponent} from "./components/adm-dashboard/adm-dashboard.component";
import {AdmProfileComponent} from "./components/adm-profile/adm-profile.component";
import {AdmBlankComponent} from "./components/adm-blank/adm-blank.component";
import {AdmToolButtonComponent} from "./components/adm-tool-button/adm-tool-button.component";
import {AdmToolCardsComponent} from "./components/adm-tool-cards/adm-tool-cards.component";
import {AdmToolFormsComponent} from "./components/adm-tool-forms/adm-tool-forms.component";
import {AdmToolIconsComponent} from "./components/adm-tool-icons/adm-tool-icons.component";
import {AdmToolTypographyComponent} from "./components/adm-tool-typography/adm-tool-typography.component";
import {AdmPluginChartsComponent} from "./components/adm-plugin-charts/adm-plugin-charts.component";
import {AdmPluginMapsComponent} from "./components/adm-plugin-maps/adm-plugin-maps.component";
import {AdmUpgradeToProComponent} from "./components/adm-upgrade-to-pro/adm-upgrade-to-pro.component";

const routes: Routes = [
  {
    path: 'dashboard',
    component: AdmDashboardComponent
  },
  {
    path: 'profile',
    component: AdmProfileComponent
  },
  {
    path: 'blank-page',
    component: AdmBlankComponent
  },
  {
    path: 'buttons',
    component: AdmToolButtonComponent
  },
  {
    path: 'cards',
    component: AdmToolCardsComponent
  },
  {
    path: 'forms',
    component: AdmToolFormsComponent
  },
  {
    path: 'icons',
    component: AdmToolIconsComponent
  },
  {
    path: 'typography',
    component: AdmToolTypographyComponent
  },
  {
    path: 'charts',
    component: AdmPluginChartsComponent
  },
  {
    path: 'maps',
    component: AdmPluginMapsComponent
  },
  {
    path: 'upgrade-to-pro',
    component: AdmUpgradeToProComponent
  },
  { path: '', redirectTo: 'dashboard', pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
