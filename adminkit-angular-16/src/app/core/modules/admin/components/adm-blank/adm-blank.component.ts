import { Component } from '@angular/core';

@Component({
  selector: 'app-adm-blank',
  template: `
    <h1 class="h3 mb-3">Blank Page</h1>
    <div class="row">
      <div class="col-12">
        <div class="card bg-white">
          <div class="card-header">
            <h5 class="card-title mb-0">Empty card</h5>
          </div>
          <div class="card-body">
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AdmBlankComponent {

}
