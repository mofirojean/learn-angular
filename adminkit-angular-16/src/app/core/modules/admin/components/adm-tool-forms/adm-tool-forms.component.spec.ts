import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmToolFormsComponent } from './adm-tool-forms.component';

describe('AdmToolFormsComponent', () => {
  let component: AdmToolFormsComponent;
  let fixture: ComponentFixture<AdmToolFormsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmToolFormsComponent]
    });
    fixture = TestBed.createComponent(AdmToolFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
