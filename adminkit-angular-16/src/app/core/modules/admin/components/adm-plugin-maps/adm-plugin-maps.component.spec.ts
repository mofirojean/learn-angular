import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmPluginMapsComponent } from './adm-plugin-maps.component';

describe('AdmPluginMapsComponent', () => {
  let component: AdmPluginMapsComponent;
  let fixture: ComponentFixture<AdmPluginMapsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmPluginMapsComponent]
    });
    fixture = TestBed.createComponent(AdmPluginMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
