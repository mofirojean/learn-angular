import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmToolCardsComponent } from './adm-tool-cards.component';

describe('AdmToolCardsComponent', () => {
  let component: AdmToolCardsComponent;
  let fixture: ComponentFixture<AdmToolCardsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmToolCardsComponent]
    });
    fixture = TestBed.createComponent(AdmToolCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
