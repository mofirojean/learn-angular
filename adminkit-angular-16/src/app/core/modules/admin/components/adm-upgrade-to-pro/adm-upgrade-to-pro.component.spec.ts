import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmUpgradeToProComponent } from './adm-upgrade-to-pro.component';

describe('AdmUpgradeToProComponent', () => {
  let component: AdmUpgradeToProComponent;
  let fixture: ComponentFixture<AdmUpgradeToProComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmUpgradeToProComponent]
    });
    fixture = TestBed.createComponent(AdmUpgradeToProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
