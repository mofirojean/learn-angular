import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmDashboardFlatpickrComponent } from './adm-dashboard-flatpickr.component';

describe('AdmDashboardFlatpickrComponent', () => {
  let component: AdmDashboardFlatpickrComponent;
  let fixture: ComponentFixture<AdmDashboardFlatpickrComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmDashboardFlatpickrComponent]
    });
    fixture = TestBed.createComponent(AdmDashboardFlatpickrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
