import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Map, NavigationControl } from 'maplibre-gl';


@Component({
  selector: 'app-adm-dashboard-vector-map',
  templateUrl: './adm-dashboard-vector-map.component.html',
  styleUrls: ['./adm-dashboard-vector-map.component.scss']
})
export class AdmDashboardVectorMapComponent implements OnInit, AfterViewInit {
  @ViewChild('map')
  private mapContainer!: ElementRef<HTMLElement>;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    // This API key is for use only in the demo of this project
    // Get your Geoapify API key on https://www.geoapify.com/get-started-with-maps-api
    // The Geoapify service is free for small projects and the development phase.
    // The API key will expire
    const myAPIKey = 'd1afd4503172498d977ac0f9d77edcba';
    const mapStyle = 'https://maps.geoapify.com/v1/styles/positron/style.json';

    const initialState = {
      lng: 12.3547,
      lat: 7.3697,
      zoom: 9,
    };

    const map = new Map({
      container: this.mapContainer.nativeElement,
      style: `${mapStyle}?apiKey=${myAPIKey}`,
      center: [initialState.lng, initialState.lat],
      zoom: initialState.zoom,
    });

    map.addControl(new NavigationControl());
  }
}
