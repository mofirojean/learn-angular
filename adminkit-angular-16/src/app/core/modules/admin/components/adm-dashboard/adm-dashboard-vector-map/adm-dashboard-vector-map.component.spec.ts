import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmDashboardVectorMapComponent } from './adm-dashboard-vector-map.component';

describe('AdmDashboardVectorMapComponent', () => {
  let component: AdmDashboardVectorMapComponent;
  let fixture: ComponentFixture<AdmDashboardVectorMapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmDashboardVectorMapComponent]
    });
    fixture = TestBed.createComponent(AdmDashboardVectorMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
