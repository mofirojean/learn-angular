import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import flatpickr from "flatpickr";
import {FlatpickrFn} from "flatpickr/dist/types/instance";

@Component({
  selector: 'app-adm-dashboard-flatpickr',
  templateUrl: './adm-dashboard-flatpickr.component.html',
  styleUrls: ['./adm-dashboard-flatpickr.component.scss']
})
export class AdmDashboardFlatpickrComponent implements AfterViewInit {
  @ViewChild('date', { static: false }) date!: ElementRef;

  ngAfterViewInit() {
    this.createFlatPickrDate()
  }

  createFlatPickrDate = () => {
    let date = new Date(Date.now() - 5 * 24 * 60 * 60 * 1000);
    let defaultDate = date.getUTCFullYear() + "-" + (date.getUTCMonth() + 1) + "-" + date.getUTCDate();
    (flatpickr as FlatpickrFn)(this.date.nativeElement, {
      inline: true,
      prevArrow: "<span title=\"Previous month\">&laquo;</span>",
      nextArrow: "<span title=\"Next month\">&raquo;</span>",
      defaultDate: defaultDate
    })
  }
}
