import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmPieChartComponent } from './adm-pie-chart.component';

describe('AdmPieChartComponent', () => {
  let component: AdmPieChartComponent;
  let fixture: ComponentFixture<AdmPieChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmPieChartComponent]
    });
    fixture = TestBed.createComponent(AdmPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
