import {Component, OnInit} from '@angular/core';
import {Chart} from "chart.js/auto";
import {themes} from "../chart.model";

@Component({
  selector: 'app-adm-line-chart',
  templateUrl: './adm-line-chart.component.html',
  styles: [
  ]
})
export class AdmLineChartComponent implements OnInit {
  public chart: any;

  ngOnInit() {
    this.createLineChartAction();
  }

  createLineChartAction = () => {
    let ctx = document.getElementById('chartjs-dashboard-line') as HTMLCanvasElement;
    let gradient = ctx.getContext("2d")!.createLinearGradient(0, 0, 0, 225);
    gradient.addColorStop(0, "rgba(215, 227, 244, 1)");
    gradient.addColorStop(1, "rgba(215, 227, 244, 0)");

    new Chart(ctx, {
      type: "line",
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
          {
            label: "Sales ($)",
            fill: true,
            backgroundColor: gradient,
            borderColor: themes['primary'],
            data: [
              2115,
              1562,
              1584,
              1892,
              1587,
              1923,
              2566,
              2448,
              2805,
              3438,
              2917,
              3327
            ]
          }
        ],
      },
      options: {
        maintainAspectRatio: false,
        hover: {
          intersect: true
        },
        plugins: {
          filler: {
            propagate: false
          }
        }
      }
    })
  }
}
