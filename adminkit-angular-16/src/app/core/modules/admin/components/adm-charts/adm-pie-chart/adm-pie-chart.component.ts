import {Component, OnInit} from '@angular/core';
import {Chart} from "chart.js/auto";
import {themes} from "../chart.model";

@Component({
  selector: 'app-adm-pie-chart',
  templateUrl: './adm-pie-chart.component.html',
  styles: [
  ]
})
export class AdmPieChartComponent implements OnInit {
  public chart: any;

  ngOnInit() {
    this.createPieChartAction();
  }

  createPieChartAction = () => {
    let ctx = document.getElementById("chartjs-dashboard-pie") as HTMLCanvasElement;
    new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ["Chrome", "Firefox", "IE"],
        datasets: [{
          data: [4306, 3801, 1689],
          backgroundColor: [
            themes['primary'],
            themes['warning'],
            themes['danger']
          ],
          borderWidth: 5
        }]
      },
      options: {
        maintainAspectRatio: false
      }
    })
  }
}
