import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmLineChartComponent } from './adm-line-chart.component';

describe('AdmLineChartComponent', () => {
  let component: AdmLineChartComponent;
  let fixture: ComponentFixture<AdmLineChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmLineChartComponent]
    });
    fixture = TestBed.createComponent(AdmLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
