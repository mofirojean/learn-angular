import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Chart} from "chart.js/auto";
import {themes} from "../chart.model";

@Component({
  selector: 'app-adm-bar-chart',
  templateUrl: './adm-bar-chart.component.html',
  styles: [
  ]
})
export class AdmBarChartComponent implements AfterViewInit {
  public chart: any;

  ngAfterViewInit() {
    this.createBarChartAction();
  }

  createBarChartAction = () => {
    let ctx = document.getElementById('chartjs-dashboard-line') as HTMLCanvasElement;
    new Chart(ctx, {
      type: "bar",
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
          {
            data: [54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79]
          }
        ]
      }
    });
  }
}
