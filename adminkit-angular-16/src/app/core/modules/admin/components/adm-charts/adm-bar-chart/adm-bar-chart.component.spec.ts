import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmBarChartComponent } from './adm-bar-chart.component';

describe('AdmBarChartComponent', () => {
  let component: AdmBarChartComponent;
  let fixture: ComponentFixture<AdmBarChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmBarChartComponent]
    });
    fixture = TestBed.createComponent(AdmBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
