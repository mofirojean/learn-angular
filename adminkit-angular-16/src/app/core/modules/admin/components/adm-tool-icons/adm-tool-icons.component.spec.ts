import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmToolIconsComponent } from './adm-tool-icons.component';

describe('AdmToolIconsComponent', () => {
  let component: AdmToolIconsComponent;
  let fixture: ComponentFixture<AdmToolIconsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmToolIconsComponent]
    });
    fixture = TestBed.createComponent(AdmToolIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
