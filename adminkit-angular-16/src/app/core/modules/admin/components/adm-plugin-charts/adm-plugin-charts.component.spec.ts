import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmPluginChartsComponent } from './adm-plugin-charts.component';

describe('AdmPluginChartsComponent', () => {
  let component: AdmPluginChartsComponent;
  let fixture: ComponentFixture<AdmPluginChartsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmPluginChartsComponent]
    });
    fixture = TestBed.createComponent(AdmPluginChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
