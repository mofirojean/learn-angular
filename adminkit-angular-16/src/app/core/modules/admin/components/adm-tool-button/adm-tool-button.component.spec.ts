import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmToolButtonComponent } from './adm-tool-button.component';

describe('AdmToolButtonComponent', () => {
  let component: AdmToolButtonComponent;
  let fixture: ComponentFixture<AdmToolButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmToolButtonComponent]
    });
    fixture = TestBed.createComponent(AdmToolButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
