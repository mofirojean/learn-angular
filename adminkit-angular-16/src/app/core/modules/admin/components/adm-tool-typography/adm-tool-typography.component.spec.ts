import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmToolTypographyComponent } from './adm-tool-typography.component';

describe('AdmToolTypographyComponent', () => {
  let component: AdmToolTypographyComponent;
  let fixture: ComponentFixture<AdmToolTypographyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmToolTypographyComponent]
    });
    fixture = TestBed.createComponent(AdmToolTypographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
