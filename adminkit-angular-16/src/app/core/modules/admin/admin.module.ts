import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdmDashboardComponent } from './components/adm-dashboard/adm-dashboard.component';
import { AdmProfileComponent } from './components/adm-profile/adm-profile.component';
import { AdmBlankComponent } from './components/adm-blank/adm-blank.component';
import { AdmToolButtonComponent } from './components/adm-tool-button/adm-tool-button.component';
import { AdmToolFormsComponent } from './components/adm-tool-forms/adm-tool-forms.component';
import { AdmToolCardsComponent } from './components/adm-tool-cards/adm-tool-cards.component';
import { AdmToolTypographyComponent } from './components/adm-tool-typography/adm-tool-typography.component';
import { AdmToolIconsComponent } from './components/adm-tool-icons/adm-tool-icons.component';
import { AdmPluginChartsComponent } from './components/adm-plugin-charts/adm-plugin-charts.component';
import { AdmPluginMapsComponent } from './components/adm-plugin-maps/adm-plugin-maps.component';
import {LibraryModule} from "../../library/library.module";
import {IconsModule} from "../../library/icons/icons.module";
import { AdmUpgradeToProComponent } from './components/adm-upgrade-to-pro/adm-upgrade-to-pro.component';
import { AdmLineChartComponent } from './components/adm-charts/adm-line-chart/adm-line-chart.component';
import { AdmPieChartComponent } from './components/adm-charts/adm-pie-chart/adm-pie-chart.component';
import { AdmBarChartComponent } from './components/adm-charts/adm-bar-chart/adm-bar-chart.component';
import { AdmDashboardFlatpickrComponent } from './components/adm-dashboard/adm-dashboard-flatpickr/adm-dashboard-flatpickr.component';
import { AdmDashboardVectorMapComponent } from './components/adm-dashboard/adm-dashboard-vector-map/adm-dashboard-vector-map.component';


@NgModule({
  declarations: [
    AdmDashboardComponent,
    AdmProfileComponent,
    AdmBlankComponent,
    AdmToolButtonComponent,
    AdmToolFormsComponent,
    AdmToolCardsComponent,
    AdmToolTypographyComponent,
    AdmToolIconsComponent,
    AdmPluginChartsComponent,
    AdmPluginMapsComponent,
    AdmUpgradeToProComponent,
    AdmLineChartComponent,
    AdmPieChartComponent,
    AdmBarChartComponent,
    AdmDashboardFlatpickrComponent,
    AdmDashboardVectorMapComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    LibraryModule,
    IconsModule
  ]
})
export class AdminModule { }
