import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from "./components/layout/layout.component";

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./core/modules/authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: 'app',
    component: LayoutComponent,
    children: [
      {
        path: 'admin',
        loadChildren: () => import('./core/modules/admin/admin.module').then(m => m.AdminModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
