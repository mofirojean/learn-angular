export interface CardModel {
  img: string
  name: string
  gprice: string
  dprice: string
}

export const CardItems: CardModel[] = [
  {
    img: 'assets/images/products/s4.jpg',
    name: 'Boat Headphone',
    gprice: '$50',
    dprice: '$65'
  },
  {
    img: 'assets/images/products/s5.jpg',
    name: 'MacBook Air Pro',
    gprice: '$650',
    dprice: '$900'
  },
  {
    img: 'assets/images/products/s7.jpg',
    name: 'Red Valvet Dress',
    gprice: '$150',
    dprice: '$200'
  },
  {
    img: 'assets/images/products/s11.jpg',
    name: 'Cute Soft Teddybear',
    gprice: '$285',
    dprice: '$345'
  }
]
