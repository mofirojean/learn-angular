import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmDashboardComponent } from "./components/adm-dashboard/adm-dashboard.component";
import { AdmExtraIconComponent } from "./components/adm-extra-icon/adm-extra-icon.component";
import { AdmExtraSamplePageComponent } from "./components/adm-extra-sample-page/adm-extra-sample-page.component";
import { AdmUiAlertsComponent } from "./components/adm-ui-alerts/adm-ui-alerts.component";
import { AdmUiButtonsComponent } from "./components/adm-ui-buttons/adm-ui-buttons.component";
import { AdmUiCardComponent } from "./components/adm-ui-card/adm-ui-card.component";
import { AdmUiFormComponent } from "./components/adm-ui-form/adm-ui-form.component";
import { AdmUiTypographyComponent } from "./components/adm-ui-typography/adm-ui-typography.component";

const routes: Routes = [
  {
    path: 'dashboard',
    component: AdmDashboardComponent
  },
  {
    path: 'icons',
    component: AdmExtraIconComponent
  },
  {
    path: 'sample-page',
    component: AdmExtraSamplePageComponent
  },
  {
    path: 'alerts',
    component: AdmUiAlertsComponent
  },
  {
    path: 'buttons',
    component: AdmUiButtonsComponent
  },
  {
    path: 'card',
    component: AdmUiCardComponent
  },
  {
    path: 'forms',
    component: AdmUiFormComponent
  },
  {
    path: 'typography',
    component: AdmUiTypographyComponent
  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
