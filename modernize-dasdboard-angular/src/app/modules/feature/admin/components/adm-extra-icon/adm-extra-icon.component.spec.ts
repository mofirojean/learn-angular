import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmExtraIconComponent } from './adm-extra-icon.component';

describe('AdmExtraIconComponent', () => {
  let component: AdmExtraIconComponent;
  let fixture: ComponentFixture<AdmExtraIconComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmExtraIconComponent]
    });
    fixture = TestBed.createComponent(AdmExtraIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
