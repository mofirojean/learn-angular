import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmUiTypographyComponent } from './adm-ui-typography.component';

describe('AdmUiTypographyComponent', () => {
  let component: AdmUiTypographyComponent;
  let fixture: ComponentFixture<AdmUiTypographyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmUiTypographyComponent]
    });
    fixture = TestBed.createComponent(AdmUiTypographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
