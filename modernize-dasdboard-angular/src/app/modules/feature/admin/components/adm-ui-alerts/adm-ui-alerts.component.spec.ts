import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmUiAlertsComponent } from './adm-ui-alerts.component';

describe('AdmUiAlertsComponent', () => {
  let component: AdmUiAlertsComponent;
  let fixture: ComponentFixture<AdmUiAlertsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmUiAlertsComponent]
    });
    fixture = TestBed.createComponent(AdmUiAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
