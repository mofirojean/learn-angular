import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmUiFormComponent } from './adm-ui-form.component';

describe('AdmUiFormComponent', () => {
  let component: AdmUiFormComponent;
  let fixture: ComponentFixture<AdmUiFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmUiFormComponent]
    });
    fixture = TestBed.createComponent(AdmUiFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
