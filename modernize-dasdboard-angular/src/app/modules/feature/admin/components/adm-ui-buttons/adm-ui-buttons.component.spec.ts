import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmUiButtonsComponent } from './adm-ui-buttons.component';

describe('AdmUiButtonsComponent', () => {
  let component: AdmUiButtonsComponent;
  let fixture: ComponentFixture<AdmUiButtonsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmUiButtonsComponent]
    });
    fixture = TestBed.createComponent(AdmUiButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
