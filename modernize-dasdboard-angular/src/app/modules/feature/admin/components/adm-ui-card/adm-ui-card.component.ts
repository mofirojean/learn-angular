import { Component } from '@angular/core';
import {CardItems, CardModel} from "../../../../../models/card/card.model";
import {formatImageAltText} from "../../../../../utils/format.utils";

@Component({
  selector: 'app-adm-ui-card',
  templateUrl: './adm-ui-card.component.html',
  styles: [
  ]
})
export class AdmUiCardComponent {
  cards: CardModel[] = CardItems;
  protected readonly formatImageAltText = formatImageAltText;
}

