import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmUiCardComponent } from './adm-ui-card.component';

describe('AdmUiCardComponent', () => {
  let component: AdmUiCardComponent;
  let fixture: ComponentFixture<AdmUiCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmUiCardComponent]
    });
    fixture = TestBed.createComponent(AdmUiCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
