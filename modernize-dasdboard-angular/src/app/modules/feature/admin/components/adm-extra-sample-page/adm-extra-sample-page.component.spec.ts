import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmExtraSamplePageComponent } from './adm-extra-sample-page.component';

describe('AdmExtraSamplePageComponent', () => {
  let component: AdmExtraSamplePageComponent;
  let fixture: ComponentFixture<AdmExtraSamplePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmExtraSamplePageComponent]
    });
    fixture = TestBed.createComponent(AdmExtraSamplePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
