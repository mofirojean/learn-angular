import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdmDashboardComponent } from './components/adm-dashboard/adm-dashboard.component';
import { AdmUiButtonsComponent } from './components/adm-ui-buttons/adm-ui-buttons.component';
import { AdmUiAlertsComponent } from './components/adm-ui-alerts/adm-ui-alerts.component';
import { AdmUiCardComponent } from './components/adm-ui-card/adm-ui-card.component';
import { AdmUiFormComponent } from './components/adm-ui-form/adm-ui-form.component';
import { AdmUiTypographyComponent } from './components/adm-ui-typography/adm-ui-typography.component';
import { AdmExtraIconComponent } from './components/adm-extra-icon/adm-extra-icon.component';
import { AdmExtraSamplePageComponent } from './components/adm-extra-sample-page/adm-extra-sample-page.component';
import {IconsModule} from "../../library/icons/icons.module";
import {NgApexchartsModule} from "ng-apexcharts";


@NgModule({
  declarations: [
    AdmDashboardComponent,
    AdmUiButtonsComponent,
    AdmUiAlertsComponent,
    AdmUiCardComponent,
    AdmUiFormComponent,
    AdmUiTypographyComponent,
    AdmExtraIconComponent,
    AdmExtraSamplePageComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    IconsModule,
    NgApexchartsModule
  ]
})
export class AdminModule { }
