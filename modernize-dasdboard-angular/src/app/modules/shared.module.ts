import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LibraryModule} from "./library/library.module";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LibraryModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
