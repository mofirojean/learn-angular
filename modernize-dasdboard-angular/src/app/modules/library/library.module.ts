import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './navigations/header/header.component';
import { FooterComponent } from './navigations/footer/footer.component';
import { SidebarComponent } from './navigations/sidebar/sidebar.component';
import {IconsModule} from "./icons/icons.module";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterLink, RouterLinkActive} from "@angular/router";



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    ReactiveFormsModule,
    RouterLink,
    RouterLinkActive
  ]
})
export class LibraryModule { }
