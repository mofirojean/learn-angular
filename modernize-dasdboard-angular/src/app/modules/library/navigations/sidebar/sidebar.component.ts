import {Component} from '@angular/core';
import {NavItem, NavItems} from "../../../../utils/nav.utils";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent {
  navItems: NavItem[] = NavItems;
}
