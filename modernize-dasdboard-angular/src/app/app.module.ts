import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from "./modules/shared.module";
import { LayoutComponent } from './components/layout/layout.component';
import { IconsModule } from "./modules/library/icons/icons.module";
import { NgApexchartsModule } from "ng-apexcharts";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LibraryModule } from "./modules/library/library.module";

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgApexchartsModule,
    SharedModule,
    IconsModule,
    LibraryModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
