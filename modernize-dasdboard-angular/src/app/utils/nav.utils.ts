export interface NavItem {
  caption: string
  items: NavItemType[]
}

export type NavItemType = {
  label: string
  icon: string
  link: string
}

export const NavItems: NavItem[] = [
  {
    caption: 'home',
    items: [
      {
        label: 'Dashboard',
        icon: 'layout-dashboard',
        link: '/app/admin/dashboard'
      }
    ]
  },
  {
    caption: 'Ui Components',
    items: [
      {
        label: 'Buttons',
        icon: 'article',
        link: '/app/admin/buttons'
      },
      {
        label: 'Alerts',
        icon: 'alert-circle',
        link: '/app/admin/alerts'
      },
      {
        label: 'Card',
        icon: 'cards',
        link: '/app/admin/card'
      },
      {
        label: 'Forms',
        icon: 'file-description',
        link: '/app/admin/forms'
      },
      {
        label: 'Typography',
        icon: 'typography',
        link: '/app/admin/typography'
      },
    ]
  },
  {
    caption: 'Auth',
    items: [
      {
        label: 'Register',
        icon: 'user-plus',
        link: '/auth/register'
      },
      {
        label: 'Login',
        icon: 'login',
        link: '/auth/login'
      }
    ]
  },
  {
    caption: 'Extra',
    items: [
      {
        label: 'Icons',
        icon: 'mood-happy',
        link: '/app/admin/icons'
      },
      {
        label: 'Sample page',
        icon: 'aperture',
        link: '/app/admin/sample-page'
      }
    ]
  }
]
