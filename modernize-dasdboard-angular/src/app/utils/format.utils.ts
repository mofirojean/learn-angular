export const formatImageAltText = (name: string)  => {
  return name.split(' ').map(s => s.toLowerCase()).join('-');
}
