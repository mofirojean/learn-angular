import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from "./components/layout/layout.component";

const routes: Routes = [
  {
    path: 'app',
    component: LayoutComponent,
    children: [
      {
        path: 'admin',
        loadChildren: () => import('./modules/feature/admin/admin.module').then(m => m.AdminModule)
      }
    ]
  },
  {
    path: 'auth',
    loadChildren: () => import('./modules/feature/authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: '', redirectTo: '/app/admin', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
